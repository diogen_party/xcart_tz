<?php

namespace XLite\Module\Bereg\ProductRecommendations\View\ItemsList\Model;

class QuickMessage extends \XLite\View\ItemsList\Model\Table
{
    protected function defineRepositoryName()
    {
        return 'XLite\Module\Bereg\ProductRecommendations\Model\QuickMessage';
    }

    protected function isSwitchable()
    {
        return true;
    }

    protected function isRemoved()
    {
        return true;
    }

    protected function defineColumns()
    {
        return array(
            'body' => array(
                static::COLUMN_CLASS    => 'XLite\View\FormField\Inline\Input\Text',
                static::COLUMN_NAME     => static::t('Quote text'),
                static::COLUMN_ORDERBY  => 100,
            ),

        );
    }

    protected function isInlineCreation()
    {
        return static::CREATE_INLINE_BOTTOM;
    }

    protected function getCreateURL()
    {
        return \XLite\Core\Converter::buildUrl('quotes');
    }

    protected function wrapWithFormByDefault()
    {
        return true;
    }

    protected function getFormTarget()
    {
        return 'quotes';
    }    
}