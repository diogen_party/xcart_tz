<?php

namespace XLite\Module\Bereg\ProductRecommendations\View\Page\Admin;

/**
 * QuickMessagesPage
 *
 * @ListChild (list="admin.center", zone="admin")
 */
class QuickMessagesPage extends \XLite\View\AView
{     
    /**
     * Return list of allowed targets
     */
    public static function getAllowedTargets()
    {
        return array_merge(parent::getAllowedTargets(), array('quotes'));
    }
      
    /**
     * Return widget default template
     */
    public function getDefaultTemplate()
    {
        return 'modules/Bereg/ProductRecommendations/page/quick_messages/body.twig';
    }
}