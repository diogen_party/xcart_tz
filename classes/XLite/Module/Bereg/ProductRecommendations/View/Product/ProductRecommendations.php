<?php

namespace XLite\Module\Bereg\ProductRecommendations\View\Product;

/**
 * Customer attachments input on product page
 *
 * @ListChild (list="product.details.page.info", weight="45")
 * @ListChild (list="product.details.quicklook.info", weight="65")
 */
class ProductRecommendations extends \XLite\View\AView
{
    /**
     * Product model
     *
     * @var \XLite\Model\Product
     */
    protected $product;

    /**
     * Return widget default template
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'modules/Bereg/ProductRecommendations/product/details/quotes.twig';
    }

    /**
     * Get product model
     *
     * @return \XLite\Model\Product
     */
    protected function getProduct()
    {
        if (!isset($this->product)) {
            $productId = \XLite\Core\Request::getInstance()->product_id;
            $this->product = \XLite\Core\Database::getRepo('XLite\Model\Product')->find($productId);
        }

        return $this->product;
    }
}
