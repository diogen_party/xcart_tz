<?php

namespace XLite\Module\Bereg\ProductRecommendations\View;

class QuickMessageMenu extends \XLite\View\SideBarBox
{
    protected function getHead()
    {
        return 'Store quick messages';
    }

    protected function getDir()
    {
        return 'modules/Bereg/ProductRecommendations/quickmessage';
    }

    protected function getMessages()
    {
        $return = \XLite\Core\Database::getRepo('\XLite\Module\Bereg\ProductRecommendations\Model\QuickMessage')->findNewest();

        return $return;
    }
}
