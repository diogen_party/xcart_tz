<?php

namespace XLite\Module\Bereg\ProductRecommendations\Controller\Admin;

/**
 * Product
 */
abstract class Product extends \XLite\Controller\Admin\Product implements \XLite\Base\IDecorator
{
    public function getPages()
    {
        $list = parent::getPages();

        $list['custom_tab'] = 'Recommendations';

        return $list;
    }

    protected function getPageTemplates()
    {
        $list = parent::getPageTemplates();

        $list['custom_tab'] = 'modules/Bereg/ProductRecommendations/page/tab_recommends/body.twig';

        return $list;
    }
}
