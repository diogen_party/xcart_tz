<?php

namespace XLite\Module\Bereg\ProductRecommendations\Model\Repo;

class QuickMessage extends \XLite\Model\Repo\ARepo
{  
    public function findNewest()
    {
        return $this->createQueryBuilder('qm')
            ->andWhere('qm.enabled = 1')
            ->addOrderBy('qm.id', 'DESC')
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->getResult();
    }    
}